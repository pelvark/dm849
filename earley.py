import sys

#The item tuple has form:
#(lhs,rhs,dotindex,sourcelist)

#####################################################################################################
#                                       SCANNER                                                     #
# The scanner copies all rules in the previous list where the input character                       #
# matches the character after the dot. ignores rules to empty strings.                              #
#####################################################################################################
def scanner(a,i):
    result = set()
    for item in l[i]:
        if (item[1]!="" and item[2] != len(item[1]) and item[1][item[2]] == a):
            result.add((item[0],item[1],item[2]+1,item[3]))
    return result

#####################################################################################################
#                                       PREDICTOR                                                   #
# The predictor finds all rules in the list with a dot before a non-terminal and adds all rules     #
# which matches its left hand side with said non-terminal to the list.                              #
# if the rule is completed it is ignored by the predictor                                           #
# lastly it is made sure that a rule is only added if it is not present in the list already.        #
#####################################################################################################
def predictor(j):
    changes = False
    tmp = set()

    for item in l[j]:
        if (item[2] != len(item[1]) and item[1][item[2]].isupper()):
                for rule in rules[item[1][item[2]]]:
                    tmp.add((item[1][item[2]],rule[0],0,j))
    
    if not (l[j]>=tmp):
        changes = True
        l[j]=l[j]|tmp
    return changes

#####################################################################################################
#                                       COMPLETER                                                   #
# the completer finds all completed items (where the dot is at the end.) and adds the rule that     #
# predicted this rule to the list, the dot is moved one to the right in this added rule.            #
# if the nominee rule has a dot at the end it is not checked.                                       #
# lastly it is made sure that a rule is only added if it is not present in the list already.        #
#####################################################################################################
def completer(j):
    changes = False
    tmp = set()
    for item in l[j]:
        if (item[2] == len(item[1])):
            for nominee in l[item[3]]:
                if (nominee[2] != len(nominee[1]) and nominee[1][nominee[2]] == item[0]):
                    tmp.add((nominee[0],nominee[1],nominee[2]+1,nominee[3]))

    if not (l[j]>=tmp):
        changes = True
        l[j]=l[j]|tmp
    return changes

#############################################################################################################
#                                           CREATE_PARSETREE                                                #
# create_parsetree finds all rules used for the input string and counts how many times each rule is used.   #
#                                                                                                           #
# This is done by adding the root to a set. Then while the set is not empty, The current list is searched   #
# for completed rules that could be the reason the char was "passed" by the dot.                            #
# for a rule to be the reason for a "pass" it has to come from a list where the original rule               #
# exists but with the dot on the other side of the non-terminal.                                            #
#                                                                                                           #
# After a reason rule is found we change the current list we search through to where the reason             #
# rule came from.                                                                                           #
#                                                                                                           #
# Any rules found to be the reason for a pass, is added to the set (the tree) and is checked themselves.    #
# If a terminal is encountered, we change the list we search through to the previous list.                  #
#############################################################################################################
def create_parsetree(input_string):
    s = set()
    n = len(input_string)

    # Add root.
    for item in l[n]:
        if (item[2] == len(item[1]) and item[3]==0):
            s.add((item,n))

    
    while(s):
        item, current_list = s.pop()
        # Count rule
        for rule in rules[item[0]]:
            if rule[0] == item[1]:
                rule[1] += 1
        
        dot = item[2]-1
        while (dot>=0):
            # If non-terminal find reason rule
            if (item[1][dot].isupper()):
                matches = [x for x in l[current_list] if (x[0]==item[1][dot] and x[2]==len(x[1]) and x != item)]
                for match in matches:
                    if ((item[0],item[1],dot,item[3]) in l[match[3]]):
                        s.add((match,current_list))
                        current_list = match[3]
                        break

            # If terminal, got to previous list
            else:
                current_list = current_list -1

            dot = dot -1


#################################################################
#                       READ_INPUT                              #
# read_input just reads the input files and makes a dictionary  #
# of rules and a list of input strings                          #
#################################################################
def read_input(g_filename,s_filename):
    grammarfile = open(g_filename)

    grammars = grammarfile.read()
    for line in grammars.split("\n")[0:-1]:
        line = line.replace(" ", "")
        if (line!="" and line[0].isupper()):
            rules.setdefault(line[0], []).append([line[2:len(line)],0])

    list_of_strings = []
    string_file = open(s_filename)
    strings = string_file.read()
    for line in strings.split("\n")[0:-1]:
        list_of_strings.append(line)

    return list_of_strings
#############################################################
#                       INSERT_RULES                        #
# Insert rules initiates the set of rules from the grammar  #
#############################################################
def insert_rules(a):
    new_set = set()
    for rule in rules[a]:
        newtuple = (a,rule[0],0,0)
        new_set.add(newtuple)
    return new_set


#########################################################################################
#                                       EARLEY                                          #
# Earley is the algorithm given in slides, with the change that the lists for each      #
# level is a set instead, and a boolean "changes" keeps track of whether the while      #
# loop should keep going.                                                               #
#########################################################################################
def earley(input_string):
        l.append(insert_rules("S"))
        for j in range(1,len(input_string)+1):
            l.append(scanner(input_string[j-1],j-1))
            changes = True

            while(changes==True):
                changes = False
                changes = completer(j)
                if (predictor(j)):
                    changes = True

        for item in l[len(input_string)]:
            if (item[2] == len(item[1]) and item[3]==0):
                return True
        return False



if  __name__=="__main__":
    if len(sys.argv)<3:
        print("The program needs to be called with a grammar file as first argument and a test file as second argument.")
        sys.exit(1)

    rules = dict()

    script, grammarfile, input_file = sys.argv
    list_of_input = read_input(grammarfile,input_file)

    ############################################
    # Parse each input and count rules used    #
    ############################################
    for i in range(0,len(list_of_input)):
        l = []
        if(earley(list_of_input[i])):
            print("input on line ",i+1," accepted, counting rules used")

            create_parsetree(list_of_input[i])
        else:
            print("input not accepted, ignoring this input in the count")


    #####################################
    # Print frequencies of each rule    #
    #####################################
    for key in rules:
        counts=[]
        total=0
        for rule in rules[key]:
            print(key,"->",rule[0], " - count: ",rule[1])
            total += rule[1]

        print("The frequencies of rules with left hand side ", key)
        for rule in rules[key]:
            if total !=0:
                #print(key,"->",rule[0], " frequency: ",rule[1],"/",total)
                print(key,"->",rule[0], " frequency: ",rule[1]/total)
            else:
                print(key,"->",rule[0], " frequency: ","0")

